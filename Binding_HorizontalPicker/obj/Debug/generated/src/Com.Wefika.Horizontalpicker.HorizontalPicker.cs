using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Wefika.Horizontalpicker {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']"
	[global::Android.Runtime.Register ("com/wefika/horizontalpicker/HorizontalPicker", DoNotGenerateAcw=true)]
	public partial class HorizontalPicker : global::Android.Views.View {

		// Metadata.xml XPath class reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker.Marquee']"
		[global::Android.Runtime.Register ("com/wefika/horizontalpicker/HorizontalPicker$Marquee", DoNotGenerateAcw=true)]
		public sealed partial class Marquee : global::Android.OS.Handler {

			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/wefika/horizontalpicker/HorizontalPicker$Marquee", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (Marquee); }
			}

			internal Marquee (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		}

		// Metadata.xml XPath interface reference: path="/api/package[@name='com.wefika.horizontalpicker']/interface[@name='HorizontalPicker.OnItemClicked']"
		[Register ("com/wefika/horizontalpicker/HorizontalPicker$OnItemClicked", "", "Com.Wefika.Horizontalpicker.HorizontalPicker/IOnItemClickedInvoker")]
		public partial interface IOnItemClicked : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='com.wefika.horizontalpicker']/interface[@name='HorizontalPicker.OnItemClicked']/method[@name='onItemClicked' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("onItemClicked", "(I)V", "GetOnItemClicked_IHandler:Com.Wefika.Horizontalpicker.HorizontalPicker/IOnItemClickedInvoker, Binding_HorizontalPicker")]
			void OnItemClicked (int p0);

		}
        

        [global::Android.Runtime.Register ("com/wefika/horizontalpicker/HorizontalPicker$OnItemClicked", DoNotGenerateAcw=true)]
		internal class IOnItemClickedInvoker : global::Java.Lang.Object, IOnItemClicked {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/wefika/horizontalpicker/HorizontalPicker$OnItemClicked");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IOnItemClickedInvoker); }
			}

			IntPtr class_ref;

			public static IOnItemClicked GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IOnItemClicked> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.wefika.horizontalpicker.HorizontalPicker.OnItemClicked"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IOnItemClickedInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_onItemClicked_I;
#pragma warning disable 0169
			static Delegate GetOnItemClicked_IHandler ()
			{
				if (cb_onItemClicked_I == null)
					cb_onItemClicked_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_OnItemClicked_I);
				return cb_onItemClicked_I;
			}

			static void n_OnItemClicked_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Wefika.Horizontalpicker.HorizontalPicker.IOnItemClicked __this = global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker.IOnItemClicked> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnItemClicked (p0);
			}
#pragma warning restore 0169

			IntPtr id_onItemClicked_I;
			public unsafe void OnItemClicked (int p0)
			{
				if (id_onItemClicked_I == IntPtr.Zero)
					id_onItemClicked_I = JNIEnv.GetMethodID (class_ref, "onItemClicked", "(I)V");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onItemClicked_I, __args);
			}

		}


		// Metadata.xml XPath interface reference: path="/api/package[@name='com.wefika.horizontalpicker']/interface[@name='HorizontalPicker.OnItemSelected']"
		[Register ("com/wefika/horizontalpicker/HorizontalPicker$OnItemSelected", "", "Com.Wefika.Horizontalpicker.HorizontalPicker/IOnItemSelectedInvoker")]
		public partial interface IOnItemSelected : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='com.wefika.horizontalpicker']/interface[@name='HorizontalPicker.OnItemSelected']/method[@name='onItemSelected' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("onItemSelected", "(I)V", "GetOnItemSelected_IHandler:Com.Wefika.Horizontalpicker.HorizontalPicker/IOnItemSelectedInvoker, Binding_HorizontalPicker")]
			void OnItemSelected (int p0);

		}

		[global::Android.Runtime.Register ("com/wefika/horizontalpicker/HorizontalPicker$OnItemSelected", DoNotGenerateAcw=true)]
		internal class IOnItemSelectedInvoker : global::Java.Lang.Object, IOnItemSelected {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/wefika/horizontalpicker/HorizontalPicker$OnItemSelected");

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IOnItemSelectedInvoker); }
			}

			IntPtr class_ref;

			public static IOnItemSelected GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<IOnItemSelected> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.wefika.horizontalpicker.HorizontalPicker.OnItemSelected"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public IOnItemSelectedInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (((global::Java.Lang.Object) this).Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			static Delegate cb_onItemSelected_I;
#pragma warning disable 0169
			static Delegate GetOnItemSelected_IHandler ()
			{
				if (cb_onItemSelected_I == null)
					cb_onItemSelected_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_OnItemSelected_I);
				return cb_onItemSelected_I;
			}

			static void n_OnItemSelected_I (IntPtr jnienv, IntPtr native__this, int p0)
			{
				global::Com.Wefika.Horizontalpicker.HorizontalPicker.IOnItemSelected __this = global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker.IOnItemSelected> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.OnItemSelected (p0);
			}
#pragma warning restore 0169

			IntPtr id_onItemSelected_I;
			public unsafe void OnItemSelected (int p0)
			{
				if (id_onItemSelected_I == IntPtr.Zero)
					id_onItemSelected_I = JNIEnv.GetMethodID (class_ref, "onItemSelected", "(I)V");
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_onItemSelected_I, __args);
			}

		}


		// Metadata.xml XPath class reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker.SavedState']"
		[global::Android.Runtime.Register ("com/wefika/horizontalpicker/HorizontalPicker$SavedState", DoNotGenerateAcw=true)]
		public partial class SavedState : global::Android.Views.View.BaseSavedState {


			static IntPtr CREATOR_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker.SavedState']/field[@name='CREATOR']"
			[Register ("CREATOR")]
			public static global::Android.OS.IParcelableCreator Creator {
				get {
					if (CREATOR_jfieldId == IntPtr.Zero)
						CREATOR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CREATOR", "Landroid/os/Parcelable$Creator;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CREATOR_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Android.OS.IParcelableCreator> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static new IntPtr java_class_handle;
			internal static new IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/wefika/horizontalpicker/HorizontalPicker$SavedState", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (SavedState); }
			}

			protected SavedState (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor_Landroid_os_Parcelable_;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker.SavedState']/constructor[@name='HorizontalPicker.SavedState' and count(parameter)=1 and parameter[1][@type='android.os.Parcelable']]"
			[Register (".ctor", "(Landroid/os/Parcelable;)V", "")]
			public unsafe SavedState (global::Android.OS.IParcelable p0)
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
					return;

				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (p0);
					if (((object) this).GetType () != typeof (SavedState)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/os/Parcelable;)V", __args),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/os/Parcelable;)V", __args);
						return;
					}

					if (id_ctor_Landroid_os_Parcelable_ == IntPtr.Zero)
						id_ctor_Landroid_os_Parcelable_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/os/Parcelable;)V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_os_Parcelable_, __args),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_os_Parcelable_, __args);
				} finally {
				}
			}

		}

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/wefika/horizontalpicker/HorizontalPicker", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (HorizontalPicker); }
		}

		protected HorizontalPicker (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/constructor[@name='HorizontalPicker' and count(parameter)=3 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.util.AttributeSet'] and parameter[3][@type='int']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "")]
		public unsafe HorizontalPicker (global::Android.Content.Context p0, global::Android.Util.IAttributeSet p1, int p2)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				if (((object) this).GetType () != typeof (HorizontalPicker)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_I, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/constructor[@name='HorizontalPicker' and count(parameter)=2 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.util.AttributeSet']]"
		[Register (".ctor", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "")]
		public unsafe HorizontalPicker (global::Android.Content.Context p0, global::Android.Util.IAttributeSet p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				if (((object) this).GetType () != typeof (HorizontalPicker)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;Landroid/util/AttributeSet;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_ == IntPtr.Zero)
					id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;Landroid/util/AttributeSet;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_Landroid_util_AttributeSet_, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_Landroid_content_Context_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/constructor[@name='HorizontalPicker' and count(parameter)=1 and parameter[1][@type='android.content.Context']]"
		[Register (".ctor", "(Landroid/content/Context;)V", "")]
		public unsafe HorizontalPicker (global::Android.Content.Context p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (((global::Java.Lang.Object) this).Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (((object) this).GetType () != typeof (HorizontalPicker)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (((object) this).GetType (), "(Landroid/content/Context;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, "(Landroid/content/Context;)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_ == IntPtr.Zero)
					id_ctor_Landroid_content_Context_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (((global::Java.Lang.Object) this).Handle, class_ref, id_ctor_Landroid_content_Context_, __args);
			} finally {
			}
		}

		static Delegate cb_getEllipsize;
#pragma warning disable 0169
		static Delegate GetGetEllipsizeHandler ()
		{
			if (cb_getEllipsize == null)
				cb_getEllipsize = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEllipsize);
			return cb_getEllipsize;
		}

		static IntPtr n_GetEllipsize (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Wefika.Horizontalpicker.HorizontalPicker __this = global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Ellipsize);
		}
#pragma warning restore 0169

		static Delegate cb_setEllipsize_Landroid_text_TextUtils_TruncateAt_;
#pragma warning disable 0169
		static Delegate GetSetEllipsize_Landroid_text_TextUtils_TruncateAt_Handler ()
		{
			if (cb_setEllipsize_Landroid_text_TextUtils_TruncateAt_ == null)
				cb_setEllipsize_Landroid_text_TextUtils_TruncateAt_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetEllipsize_Landroid_text_TextUtils_TruncateAt_);
			return cb_setEllipsize_Landroid_text_TextUtils_TruncateAt_;
		}

		static void n_SetEllipsize_Landroid_text_TextUtils_TruncateAt_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Wefika.Horizontalpicker.HorizontalPicker __this = global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Text.TextUtils.TruncateAt p0 = global::Java.Lang.Object.GetObject<global::Android.Text.TextUtils.TruncateAt> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Ellipsize = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getEllipsize;
		static IntPtr id_setEllipsize_Landroid_text_TextUtils_TruncateAt_;
		public virtual unsafe global::Android.Text.TextUtils.TruncateAt Ellipsize {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/method[@name='getEllipsize' and count(parameter)=0]"
			[Register ("getEllipsize", "()Landroid/text/TextUtils$TruncateAt;", "GetGetEllipsizeHandler")]
			get {
				if (id_getEllipsize == IntPtr.Zero)
					id_getEllipsize = JNIEnv.GetMethodID (class_ref, "getEllipsize", "()Landroid/text/TextUtils$TruncateAt;");
				try {

					if (((object) this).GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Android.Text.TextUtils.TruncateAt> (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getEllipsize), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Android.Text.TextUtils.TruncateAt> (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEllipsize", "()Landroid/text/TextUtils$TruncateAt;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/method[@name='setEllipsize' and count(parameter)=1 and parameter[1][@type='android.text.TextUtils.TruncateAt']]"
			[Register ("setEllipsize", "(Landroid/text/TextUtils$TruncateAt;)V", "GetSetEllipsize_Landroid_text_TextUtils_TruncateAt_Handler")]
			set {
				if (id_setEllipsize_Landroid_text_TextUtils_TruncateAt_ == IntPtr.Zero)
					id_setEllipsize_Landroid_text_TextUtils_TruncateAt_ = JNIEnv.GetMethodID (class_ref, "setEllipsize", "(Landroid/text/TextUtils$TruncateAt;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setEllipsize_Landroid_text_TextUtils_TruncateAt_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEllipsize", "(Landroid/text/TextUtils$TruncateAt;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getMarqueeRepeatLimit;
#pragma warning disable 0169
		static Delegate GetGetMarqueeRepeatLimitHandler ()
		{
			if (cb_getMarqueeRepeatLimit == null)
				cb_getMarqueeRepeatLimit = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetMarqueeRepeatLimit);
			return cb_getMarqueeRepeatLimit;
		}

		static int n_GetMarqueeRepeatLimit (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Wefika.Horizontalpicker.HorizontalPicker __this = global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.MarqueeRepeatLimit;
		}
#pragma warning restore 0169

		static Delegate cb_setMarqueeRepeatLimit_I;
#pragma warning disable 0169
		static Delegate GetSetMarqueeRepeatLimit_IHandler ()
		{
			if (cb_setMarqueeRepeatLimit_I == null)
				cb_setMarqueeRepeatLimit_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetMarqueeRepeatLimit_I);
			return cb_setMarqueeRepeatLimit_I;
		}

		static void n_SetMarqueeRepeatLimit_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Wefika.Horizontalpicker.HorizontalPicker __this = global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.MarqueeRepeatLimit = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getMarqueeRepeatLimit;
		static IntPtr id_setMarqueeRepeatLimit_I;
		public virtual unsafe int MarqueeRepeatLimit {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/method[@name='getMarqueeRepeatLimit' and count(parameter)=0]"
			[Register ("getMarqueeRepeatLimit", "()I", "GetGetMarqueeRepeatLimitHandler")]
			get {
				if (id_getMarqueeRepeatLimit == IntPtr.Zero)
					id_getMarqueeRepeatLimit = JNIEnv.GetMethodID (class_ref, "getMarqueeRepeatLimit", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getMarqueeRepeatLimit);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getMarqueeRepeatLimit", "()I"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/method[@name='setMarqueeRepeatLimit' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setMarqueeRepeatLimit", "(I)V", "GetSetMarqueeRepeatLimit_IHandler")]
			set {
				if (id_setMarqueeRepeatLimit_I == IntPtr.Zero)
					id_setMarqueeRepeatLimit_I = JNIEnv.GetMethodID (class_ref, "setMarqueeRepeatLimit", "(I)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setMarqueeRepeatLimit_I, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setMarqueeRepeatLimit", "(I)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getSelectedItem;
#pragma warning disable 0169
		static Delegate GetGetSelectedItemHandler ()
		{
			if (cb_getSelectedItem == null)
				cb_getSelectedItem = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetSelectedItem);
			return cb_getSelectedItem;
		}

		static int n_GetSelectedItem (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Wefika.Horizontalpicker.HorizontalPicker __this = global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.SelectedItem;
		}
#pragma warning restore 0169

		static Delegate cb_setSelectedItem_I;
#pragma warning disable 0169
		static Delegate GetSetSelectedItem_IHandler ()
		{
			if (cb_setSelectedItem_I == null)
				cb_setSelectedItem_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetSelectedItem_I);
			return cb_setSelectedItem_I;
		}

		static void n_SetSelectedItem_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Wefika.Horizontalpicker.HorizontalPicker __this = global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SelectedItem = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getSelectedItem;
		static IntPtr id_setSelectedItem_I;
		public virtual unsafe int SelectedItem {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/method[@name='getSelectedItem' and count(parameter)=0]"
			[Register ("getSelectedItem", "()I", "GetGetSelectedItemHandler")]
			get {
				if (id_getSelectedItem == IntPtr.Zero)
					id_getSelectedItem = JNIEnv.GetMethodID (class_ref, "getSelectedItem", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getSelectedItem);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSelectedItem", "()I"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/method[@name='setSelectedItem' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setSelectedItem", "(I)V", "GetSetSelectedItem_IHandler")]
			set {
				if (id_setSelectedItem_I == IntPtr.Zero)
					id_setSelectedItem_I = JNIEnv.GetMethodID (class_ref, "setSelectedItem", "(I)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setSelectedItem_I, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSelectedItem", "(I)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getSideItems;
#pragma warning disable 0169
		static Delegate GetGetSideItemsHandler ()
		{
			if (cb_getSideItems == null)
				cb_getSideItems = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetSideItems);
			return cb_getSideItems;
		}

		static int n_GetSideItems (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Wefika.Horizontalpicker.HorizontalPicker __this = global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.SideItems;
		}
#pragma warning restore 0169

		static Delegate cb_setSideItems_I;
#pragma warning disable 0169
		static Delegate GetSetSideItems_IHandler ()
		{
			if (cb_setSideItems_I == null)
				cb_setSideItems_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetSideItems_I);
			return cb_setSideItems_I;
		}

		static void n_SetSideItems_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Wefika.Horizontalpicker.HorizontalPicker __this = global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SideItems = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getSideItems;
		static IntPtr id_setSideItems_I;
		public virtual unsafe int SideItems {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/method[@name='getSideItems' and count(parameter)=0]"
			[Register ("getSideItems", "()I", "GetGetSideItemsHandler")]
			get {
				if (id_getSideItems == IntPtr.Zero)
					id_getSideItems = JNIEnv.GetMethodID (class_ref, "getSideItems", "()I");
				try {

					if (((object) this).GetType () == ThresholdType)
						return JNIEnv.CallIntMethod (((global::Java.Lang.Object) this).Handle, id_getSideItems);
					else
						return JNIEnv.CallNonvirtualIntMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSideItems", "()I"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/method[@name='setSideItems' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setSideItems", "(I)V", "GetSetSideItems_IHandler")]
			set {
				if (id_setSideItems_I == IntPtr.Zero)
					id_setSideItems_I = JNIEnv.GetMethodID (class_ref, "setSideItems", "(I)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (((object) this).GetType () == ThresholdType)
						JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setSideItems_I, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSideItems", "(I)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getValues;
#pragma warning disable 0169
		static Delegate GetGetValuesHandler ()
		{
			if (cb_getValues == null)
				cb_getValues = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetValues);
			return cb_getValues;
		}

		static IntPtr n_GetValues (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Wefika.Horizontalpicker.HorizontalPicker __this = global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetValuesFormatted ());
		}
#pragma warning restore 0169

		static IntPtr id_getValues;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/method[@name='getValues' and count(parameter)=0]"
		[Register ("getValues", "()[Ljava/lang/CharSequence;", "GetGetValuesHandler")]
		public virtual unsafe global::Java.Lang.ICharSequence[] GetValuesFormatted ()
		{
			if (id_getValues == IntPtr.Zero)
				id_getValues = JNIEnv.GetMethodID (class_ref, "getValues", "()[Ljava/lang/CharSequence;");
			try {

				if (((object) this).GetType () == ThresholdType)
					return (global::Java.Lang.ICharSequence[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod (((global::Java.Lang.Object) this).Handle, id_getValues), JniHandleOwnership.TransferLocalRef, typeof (global::Java.Lang.ICharSequence));
				else
					return (global::Java.Lang.ICharSequence[]) JNIEnv.GetArray (JNIEnv.CallNonvirtualObjectMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getValues", "()[Ljava/lang/CharSequence;")), JniHandleOwnership.TransferLocalRef, typeof (global::Java.Lang.ICharSequence));
			} finally {
			}
		}

		public string[] GetValues ()
		{
			global::Java.Lang.ICharSequence[] __result = GetValuesFormatted ();
			return CharSequence.ArrayToStringArray (__result);
		}

		static Delegate cb_setOnItemClickedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemClicked_;
#pragma warning disable 0169
		static Delegate GetSetOnItemClickedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemClicked_Handler ()
		{
			if (cb_setOnItemClickedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemClicked_ == null)
				cb_setOnItemClickedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemClicked_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOnItemClickedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemClicked_);
			return cb_setOnItemClickedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemClicked_;
		}

		static void n_SetOnItemClickedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemClicked_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Wefika.Horizontalpicker.HorizontalPicker __this = global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Wefika.Horizontalpicker.HorizontalPicker.IOnItemClicked p0 = (global::Com.Wefika.Horizontalpicker.HorizontalPicker.IOnItemClicked)global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker.IOnItemClicked> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetOnItemClickedListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setOnItemClickedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemClicked_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/method[@name='setOnItemClickedListener' and count(parameter)=1 and parameter[1][@type='com.wefika.horizontalpicker.HorizontalPicker.OnItemClicked']]"
		[Register ("setOnItemClickedListener", "(Lcom/wefika/horizontalpicker/HorizontalPicker$OnItemClicked;)V", "GetSetOnItemClickedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemClicked_Handler")]
		public virtual unsafe void SetOnItemClickedListener (global::Com.Wefika.Horizontalpicker.HorizontalPicker.IOnItemClicked p0)
		{
			if (id_setOnItemClickedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemClicked_ == IntPtr.Zero)
				id_setOnItemClickedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemClicked_ = JNIEnv.GetMethodID (class_ref, "setOnItemClickedListener", "(Lcom/wefika/horizontalpicker/HorizontalPicker$OnItemClicked;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setOnItemClickedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemClicked_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOnItemClickedListener", "(Lcom/wefika/horizontalpicker/HorizontalPicker$OnItemClicked;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setOnItemSelectedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemSelected_;
#pragma warning disable 0169
		static Delegate GetSetOnItemSelectedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemSelected_Handler ()
		{
			if (cb_setOnItemSelectedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemSelected_ == null)
				cb_setOnItemSelectedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemSelected_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOnItemSelectedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemSelected_);
			return cb_setOnItemSelectedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemSelected_;
		}

		static void n_SetOnItemSelectedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemSelected_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Wefika.Horizontalpicker.HorizontalPicker __this = global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Wefika.Horizontalpicker.HorizontalPicker.IOnItemSelected p0 = (global::Com.Wefika.Horizontalpicker.HorizontalPicker.IOnItemSelected)global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker.IOnItemSelected> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetOnItemSelectedListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setOnItemSelectedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemSelected_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/method[@name='setOnItemSelectedListener' and count(parameter)=1 and parameter[1][@type='com.wefika.horizontalpicker.HorizontalPicker.OnItemSelected']]"
		[Register ("setOnItemSelectedListener", "(Lcom/wefika/horizontalpicker/HorizontalPicker$OnItemSelected;)V", "GetSetOnItemSelectedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemSelected_Handler")]
		public virtual unsafe void SetOnItemSelectedListener (global::Com.Wefika.Horizontalpicker.HorizontalPicker.IOnItemSelected p0)
		{
			if (id_setOnItemSelectedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemSelected_ == IntPtr.Zero)
				id_setOnItemSelectedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemSelected_ = JNIEnv.GetMethodID (class_ref, "setOnItemSelectedListener", "(Lcom/wefika/horizontalpicker/HorizontalPicker$OnItemSelected;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setOnItemSelectedListener_Lcom_wefika_horizontalpicker_HorizontalPicker_OnItemSelected_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOnItemSelectedListener", "(Lcom/wefika/horizontalpicker/HorizontalPicker$OnItemSelected;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setValues_arrayLjava_lang_CharSequence_;
#pragma warning disable 0169
		static Delegate GetSetValues_arrayLjava_lang_CharSequence_Handler ()
		{
			if (cb_setValues_arrayLjava_lang_CharSequence_ == null)
				cb_setValues_arrayLjava_lang_CharSequence_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetValues_arrayLjava_lang_CharSequence_);
			return cb_setValues_arrayLjava_lang_CharSequence_;
		}

		static void n_SetValues_arrayLjava_lang_CharSequence_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Wefika.Horizontalpicker.HorizontalPicker __this = global::Java.Lang.Object.GetObject<global::Com.Wefika.Horizontalpicker.HorizontalPicker> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.ICharSequence[] p0 = (global::Java.Lang.ICharSequence[]) JNIEnv.GetArray (native_p0, JniHandleOwnership.DoNotTransfer, typeof (global::Java.Lang.ICharSequence));
			__this.SetValues (p0);
			if (p0 != null)
				JNIEnv.CopyArray (p0, native_p0);
		}
#pragma warning restore 0169

		static IntPtr id_setValues_arrayLjava_lang_CharSequence_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.wefika.horizontalpicker']/class[@name='HorizontalPicker']/method[@name='setValues' and count(parameter)=1 and parameter[1][@type='java.lang.CharSequence[]']]"
		[Register ("setValues", "([Ljava/lang/CharSequence;)V", "GetSetValues_arrayLjava_lang_CharSequence_Handler")]
		public virtual unsafe void SetValues (global::Java.Lang.ICharSequence[] p0)
		{
			if (id_setValues_arrayLjava_lang_CharSequence_ == IntPtr.Zero)
				id_setValues_arrayLjava_lang_CharSequence_ = JNIEnv.GetMethodID (class_ref, "setValues", "([Ljava/lang/CharSequence;)V");
			IntPtr native_p0 = JNIEnv.NewArray (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				if (((object) this).GetType () == ThresholdType)
					JNIEnv.CallVoidMethod (((global::Java.Lang.Object) this).Handle, id_setValues_arrayLjava_lang_CharSequence_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod (((global::Java.Lang.Object) this).Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setValues", "([Ljava/lang/CharSequence;)V"), __args);
			} finally {
				if (p0 != null) {
					JNIEnv.CopyArray (native_p0, p0);
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}
		}

		public void SetValues (string[] p0)
		{
			global::Java.Lang.ICharSequence[] jlca_p0 = CharSequence.ArrayFromStringArray(p0);
			SetValues (jlca_p0);
			if (jlca_p0 != null) foreach (global::Java.Lang.String s in jlca_p0) if (s != null) s.Dispose ();
		}

	}
}
